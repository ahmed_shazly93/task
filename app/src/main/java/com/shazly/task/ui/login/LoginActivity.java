package com.shazly.task.ui.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.shazly.task.R;
import com.shazly.task.api.RetroService;
import com.shazly.task.api.RetrofitClient;
import com.shazly.task.databinding.ActivityLoginBinding;
import com.shazly.task.ui.main.MainActivity;
import com.shazly.task.utilities.BaseActivity;
import com.shazly.task.utilities.ResourceUtil;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class LoginActivity extends BaseActivity<ActivityLoginBinding> {
    RetroService retroService;
    CompositeDisposable compositeDisposable;

    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init() {

        retroService = RetrofitClient.getClient().create(RetroService.class);
        compositeDisposable = new CompositeDisposable();
        if (ResourceUtil.getCurrentLanguage(this).equals("ar")) {
            getViewDataBinding().userEditText.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
            getViewDataBinding().passwordEditText.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);

        } else {
            getViewDataBinding().userEditText.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
            getViewDataBinding().passwordEditText.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

        }
        getViewDataBinding().loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkText()) {
                    login();
                }
            }
        });

    }


    private void login() {
        if (ResourceUtil.isNetworkAvailable(this)) {
            showProgressDialog();

            compositeDisposable.add(
                    retroService.login(getViewDataBinding().userEditText.getText().toString(),
                            getViewDataBinding().passwordEditText.getText().toString())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io()).subscribeWith
                            (new DisposableSingleObserver<LoginModel>() {
                                @Override
                                public void onSuccess(LoginModel model) {
                                    dismissProgressDialog();
                                    if (model.getData() != null) {
                                        ResourceUtil.saveToken(model.getData().getAuthorization(), LoginActivity.this);
                                        ResourceUtil.saveIsLogin(true, LoginActivity.this);
                                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                        finish();
                                    } else {
                                        showSnakeBar(model.getError());
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {
                                    dismissProgressDialog();
                                    showSnakeBar(e.getMessage());
                                }
                            }));
        } else {
            showSnakeBar(ResourceUtil.getCurrentLanguage(this).equals("en") ? "Please check your network connection" : "تحقق من اتصالك بالإنترنت ");
        }

    }

    private boolean checkText() {


        if (getViewDataBinding().userEditText.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.r_user_name), Toast.LENGTH_SHORT).show();
            return false;

        }

        if (getViewDataBinding().passwordEditText.getText().toString().isEmpty()) {
            Toast.makeText(this, getString(R.string.r_password), Toast.LENGTH_SHORT).show();
            return false;
        }


        return true;

    }
}
