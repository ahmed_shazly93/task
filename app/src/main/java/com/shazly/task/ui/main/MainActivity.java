package com.shazly.task.ui.main;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.shazly.task.R;
import com.shazly.task.databinding.ActivityMainBinding;
import com.shazly.task.ui.main.fragment.home.HomeFragment;
import com.shazly.task.ui.main.fragment.profile.ProfileFragment;
import com.shazly.task.ui.main.fragment.search.SearchFragment;
import com.shazly.task.utilities.BaseActivity;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements View.OnClickListener {
    private int currentPosition = 1;
    BottomNavigationView navView;
    private Fragment[] fragments = {
            new ProfileFragment(),
            new HomeFragment(),
            new SearchFragment()

    };
    Toolbar toolbar;
    TextView mToolbarTitle;

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear(); // Remove all existing items from the menu, leaving it empty as if it had just been created.
        getMenuInflater().inflate(R.menu.notification_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.notification_menu) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void init() {
        toolbar = findViewById(R.id.toolbar);
        mToolbarTitle = findViewById(R.id.title);
        setSupportActionBar(toolbar);
        setTitle("");
        getViewDataBinding().profile.setOnClickListener(this);
        getViewDataBinding().homePage.setOnClickListener(this);
        getViewDataBinding().search.setOnClickListener(this);
        currentPosition = getIntent().getIntExtra("position", 1);
        switchFragment(currentPosition);

    }


    private void switchFragment(int index) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        String tag = fragments[index].getClass().getName();

        if (getSupportFragmentManager().findFragmentByTag(tag) == null) {
            transaction.add(R.id.frame_container, fragments[index], tag);
        }
        transaction.hide(fragments[currentPosition]);
        transaction.show(fragments[index]);
        transaction.commit();
        currentPosition = index;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.profile:
                if (currentPosition != 0) {
                    switchFragment(0);
                    getViewDataBinding().profileImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_person_yellow));
                    getViewDataBinding().searchImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_deactive));
                }
                mToolbarTitle.setText(getString(R.string.home));
                break;
            case R.id.home_page:
                if (currentPosition != 1) {
                    switchFragment(1);
                    getViewDataBinding().profileImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                    getViewDataBinding().searchImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_deactive));
                }
                break;

            case R.id.search:
                if (currentPosition != 2) {
                    switchFragment(2);
                    getViewDataBinding().profileImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile));
                    getViewDataBinding().searchImage.setImageDrawable(getResources().getDrawable(R.drawable.ic_search));
                }
                break;

        }

    }
}
