package com.shazly.task.ui.main.fragment.home;

import android.os.Bundle;
import android.view.Gravity;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.shazly.task.R;
import com.shazly.task.api.ObjectBaseReasponse;
import com.shazly.task.api.RetroService;
import com.shazly.task.api.RetrofitClient;
import com.shazly.task.databinding.FragmentHomeBinding;
import com.shazly.task.utilities.BaseFragment;
import com.shazly.task.utilities.ResourceUtil;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment<FragmentHomeBinding> {
    RetroService retroService;
    CompositeDisposable compositeDisposable;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();

    }

    private void init() {
        retroService = RetrofitClient.getClient().create(RetroService.class);
        compositeDisposable = new CompositeDisposable();


        getProductsCount();
        getSoldCount();
    }

    private void getSoldCount() {
        if (ResourceUtil.isNetworkAvailable(getActivity())) {
            showProgressDialog();

            compositeDisposable.add(
                    retroService.getSoldCount().observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io()).subscribeWith
                            (new DisposableSingleObserver<SoldModel>() {
                                @Override
                                public void onSuccess(SoldModel model) {
                                    dismissProgressDialog();
                                    if (model.getStatus().equals("success")) {
                                        getViewDataBinding().soldItem.setText(String.valueOf(model.getData()));
                                    } else {
                                        showSnakeBar(model.getError());
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {
                                    dismissProgressDialog();
                                    showSnakeBar(e.getMessage());
                                }
                            }));
        } else {
            showSnakeBar(ResourceUtil.getCurrentLanguage(getActivity()).equals("en") ? "Please check your network connection" : "تحقق من اتصالك بالإنترنت ");
        }

    }

    private void getProductsCount() {
        if (ResourceUtil.isNetworkAvailable(getActivity())) {
            showProgressDialog();

            compositeDisposable.add(
                    retroService.getProductsCount().observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io()).subscribeWith
                            (new DisposableSingleObserver<ProductsModel>() {
                                @Override
                                public void onSuccess(ProductsModel model) {
                                    dismissProgressDialog();
                                    if (model.getStatus().equals("success")) {
                                        getViewDataBinding().product.setText(String.valueOf(model.getData()));
                                    } else {
                                        showSnakeBar(model.getError());
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {
                                    dismissProgressDialog();
                                    showSnakeBar(e.getMessage());
                                }
                            }));
        } else {
            showSnakeBar(ResourceUtil.getCurrentLanguage(getActivity()).equals("en") ? "Please check your network connection" : "تحقق من اتصالك بالإنترنت ");
        }

    }

}
