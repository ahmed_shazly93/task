package com.shazly.task.ui.main.fragment.profile;

public class UserModel {

    /**
     * data : {"id":4,"first_name":"Aly","last_name":"Mohamed","username":"Aly","photo":"3eef4c27211b94ac46c0.png","is_active":true,"deleted_at":null,"created_at":"2019-01-18 19:44:58","updated_at":"2020-03-13 19:35:13","name":"Aly Mohamed","avatar":"http://lovinmybag.beetleware.com/storage/images/users/3eef4c27211b94ac46c0.png"}
     */

    private DataBean data;
    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 4
         * first_name : Aly
         * last_name : Mohamed
         * username : Aly
         * photo : 3eef4c27211b94ac46c0.png
         * is_active : true
         * deleted_at : null
         * created_at : 2019-01-18 19:44:58
         * updated_at : 2020-03-13 19:35:13
         * name : Aly Mohamed
         * avatar : http://lovinmybag.beetleware.com/storage/images/users/3eef4c27211b94ac46c0.png
         */

        private int id;
        private String first_name;
        private String last_name;
        private String username;
        private String photo;
        private boolean is_active;
        private Object deleted_at;
        private String created_at;
        private String updated_at;
        private String name;
        private String avatar;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public boolean isIs_active() {
            return is_active;
        }

        public void setIs_active(boolean is_active) {
            this.is_active = is_active;
        }

        public Object getDeleted_at() {
            return deleted_at;
        }

        public void setDeleted_at(Object deleted_at) {
            this.deleted_at = deleted_at;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }
    }
}
