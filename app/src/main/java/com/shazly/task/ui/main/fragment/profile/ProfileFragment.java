package com.shazly.task.ui.main.fragment.profile;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shazly.task.R;
import com.shazly.task.api.RetroService;
import com.shazly.task.api.RetrofitClient;
import com.shazly.task.databinding.FragmentHomeBinding;
import com.shazly.task.databinding.FragmentProfileBinding;
import com.shazly.task.ui.login.LoginActivity;
import com.shazly.task.ui.main.fragment.home.ProductsModel;
import com.shazly.task.ui.main.fragment.home.SoldModel;
import com.shazly.task.utilities.BaseFragment;
import com.shazly.task.utilities.ResourceUtil;
import com.squareup.picasso.Picasso;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends BaseFragment<FragmentProfileBinding> {
    RetroService retroService;
    CompositeDisposable compositeDisposable;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public int getLayoutId() {
        return R.layout.fragment_profile;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();

    }

    private void init() {
        retroService = RetrofitClient.getClient().create(RetroService.class);
        compositeDisposable = new CompositeDisposable();
        getViewDataBinding().logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ResourceUtil.saveIsLogin(false, getActivity());
                startActivity(new Intent(getActivity(), LoginActivity.class));
                getActivity().finishAffinity();
            }
        });
        getprofile();
    }

    private void getprofile() {
        if (ResourceUtil.isNetworkAvailable(getActivity())) {
            showProgressDialog();

            compositeDisposable.add(
                    retroService.getprofile().observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io()).subscribeWith
                            (new DisposableSingleObserver<UserModel>() {
                                @Override
                                public void onSuccess(UserModel model) {
                                    dismissProgressDialog();
                                    if (model.getData() != null) {
                                        getViewDataBinding().name.setText(model.getData().getName());
//                                        getViewDataBinding().job.setText(model.getData().get());
                                        Picasso.get().load(model.getData().getAvatar()).into(getViewDataBinding().image);
                                    } else {
                                        showSnakeBar(model.getError());
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {
                                    dismissProgressDialog();
                                    showSnakeBar(e.getMessage());
                                }
                            }));
        } else {
            showSnakeBar(ResourceUtil.getCurrentLanguage(getActivity()).equals("en") ? "Please check your network connection" : "تحقق من اتصالك بالإنترنت ");
        }

    }

//    private void getEmployee() {
//        if (ResourceUtil.isNetworkAvailable(getActivity())) {
//            showProgressDialog();
//
//            compositeDisposable.add(
//                    retroService.getEmployee().observeOn(AndroidSchedulers.mainThread())
//                            .subscribeOn(Schedulers.io()).subscribeWith
//                            (new DisposableSingleObserver<EmployeeModel>() {
//                                @Override
//                                public void onSuccess(EmployeeModel model) {
//                                    dismissProgressDialog();
//                                    if (model.getStatus().equals("success")) {
//                                        getViewDataBinding().sales.setText(String.valueOf(model.getData().));
//                                    } else {
//                                        showSnakeBar(model.getError());
//                                    }
//                                }
//
//                                @Override
//                                public void onError(Throwable e) {
//                                    dismissProgressDialog();
//                                    showSnakeBar(e.getMessage());
//                                }
//                            }));
//        } else {
//            showSnakeBar(ResourceUtil.getCurrentLanguage(getActivity()).equals("en") ? "Please check your network connection" : "تحقق من اتصالك بالإنترنت ");
//        }
//
//    }

}
