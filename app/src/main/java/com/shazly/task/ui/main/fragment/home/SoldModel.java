package com.shazly.task.ui.main.fragment.home;

public class SoldModel {

    /**
     * data : 7
     * status : success
     * error : t
     */

    private int data;
    private String status;
    private String error;

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
