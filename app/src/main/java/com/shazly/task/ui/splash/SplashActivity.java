package com.shazly.task.ui.splash;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.shazly.task.R;
import com.shazly.task.databinding.ActivitySplashBinding;
import com.shazly.task.ui.login.LoginActivity;
import com.shazly.task.ui.main.MainActivity;
import com.shazly.task.utilities.BaseActivity;
import com.shazly.task.utilities.ResourceUtil;

public class SplashActivity extends BaseActivity<ActivitySplashBinding> {


    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // remove title
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        init();
    }

    private void init() {


        ResourceUtil.changeLang("en", SplashActivity.this);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (ResourceUtil.isLogin(SplashActivity.this)) {
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        }, 3000);
    }
//    }


}
