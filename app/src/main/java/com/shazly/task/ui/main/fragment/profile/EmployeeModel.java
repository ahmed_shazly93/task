package com.shazly.task.ui.main.fragment.profile;

public class EmployeeModel {

    /**
     * data : {"pending":62,"processing":0,"accepted":0,"returned":0,"rejected":0,"finished":0}
     * status : success
     */

    private DataBean data;
    private String status;
    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static class DataBean {
        /**
         * pending : 62
         * processing : 0
         * accepted : 0
         * returned : 0
         * rejected : 0
         * finished : 0
         */

        private int pending;
        private int processing;
        private int accepted;
        private int returned;
        private int rejected;
        private int finished;

        public int getPending() {
            return pending;
        }

        public void setPending(int pending) {
            this.pending = pending;
        }

        public int getProcessing() {
            return processing;
        }

        public void setProcessing(int processing) {
            this.processing = processing;
        }

        public int getAccepted() {
            return accepted;
        }

        public void setAccepted(int accepted) {
            this.accepted = accepted;
        }

        public int getReturned() {
            return returned;
        }

        public void setReturned(int returned) {
            this.returned = returned;
        }

        public int getRejected() {
            return rejected;
        }

        public void setRejected(int rejected) {
            this.rejected = rejected;
        }

        public int getFinished() {
            return finished;
        }

        public void setFinished(int finished) {
            this.finished = finished;
        }
    }
}
