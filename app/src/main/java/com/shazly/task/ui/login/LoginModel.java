package com.shazly.task.ui.login;

public class LoginModel {

    /**
     * success : true
     * data : {"authorization":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQsImlzcyI6Imh0dHA6Ly9sb3Zpbm15YmFnLmJlZXRsZXdhcmUuY29tL2FwaS9hdXRoIiwiaWF0IjoxNTg0MjE2NjAyLCJleHAiOjE2NjE5NzY2MDIsIm5iZiI6MTU4NDIxNjYwMiwianRpIjoiQ3dDNmxFNEN2cmF6am5yMSJ9.HJvEKpnp3M7TQIJMe1_8XTVsR_Bj7GsDo_gGaPPXmqA"}
     */

    private boolean success;
    private DataBean data;
    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * authorization : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQsImlzcyI6Imh0dHA6Ly9sb3Zpbm15YmFnLmJlZXRsZXdhcmUuY29tL2FwaS9hdXRoIiwiaWF0IjoxNTg0MjE2NjAyLCJleHAiOjE2NjE5NzY2MDIsIm5iZiI6MTU4NDIxNjYwMiwianRpIjoiQ3dDNmxFNEN2cmF6am5yMSJ9.HJvEKpnp3M7TQIJMe1_8XTVsR_Bj7GsDo_gGaPPXmqA
         */

        private String authorization;

        public String getAuthorization() {
            return authorization;
        }

        public void setAuthorization(String authorization) {
            this.authorization = authorization;
        }
    }
}
