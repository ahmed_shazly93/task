
package com.shazly.task.utilities;

import android.content.Context;
import android.content.SharedPreferences;



/**
 * Created by shazly on 14/3 /2020.
 */


public class SharedPrefManager {

    //the constants
    public static final String SHARED_PREF_NAME = "simplifiedcodingsharedpref";
    public static final String SHARED_PREF_NAME1 = "simplifiedcodingsharedpref1";

    private static final String KEY_ID = "keyid";
    private static final String KEY_NAME = "keyName";
    private static final String KEY_MOBILE = "keymobile";
    private static final String KEY_EMAIL = "keyemail";
    private static final String KEY_Token = "keytoken";
    private static final String KEY_earnings = "earnings";
    private static final String KEY_facebook = "FaceBook";
    private static final String KEY_Credit = "Credit";
    private static final String Key_competition = "Competition";
    private static final String KEY_Image = "Image";



    private static SharedPrefManager mInstance;
    private static Context mCtx;

    private SharedPrefManager(Context context) {
        mCtx = context;
    }

    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }


//    public void userLogin(UserModel.DataBean user) {
//        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putInt(KEY_ID, user.getId());
//        editor.putString(KEY_NAME, user.getName());
//        editor.putString(KEY_EMAIL, user.getEmail());
//        editor.putString(KEY_MOBILE, user.getPhone());
//        editor.putString(KEY_Image, user.getImage());
//        editor.putString(KEY_facebook, user.getFacebook_url());
//        editor.putInt(Key_competition, user.getCompetitions());
//        editor.putFloat(KEY_earnings, user.getEarnings());
//        editor.putFloat(KEY_Credit, user.getCredit());
//        editor.putString(KEY_Token, user.getToken());
//        editor.apply();
//
//    }
//
//
//
//    //    this method will give the logged in user
//    public UserModel.DataBean getUser() {
//        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
//        return new UserModel.DataBean(
//                sharedPreferences.getInt(KEY_ID, 0),
//                sharedPreferences.getString(KEY_NAME, null),
//                sharedPreferences.getString(KEY_EMAIL, null),
//                sharedPreferences.getString(KEY_MOBILE, null),
//                sharedPreferences.getString(KEY_Image, null),
//                sharedPreferences.getString(KEY_facebook, null),
//                sharedPreferences.getInt(Key_competition, 0),
//                sharedPreferences.getFloat(KEY_earnings, 0),
//                sharedPreferences.getFloat(KEY_Credit, 0),
//                sharedPreferences.getString(KEY_Token, null));
//
//    }


    //this method will logout the user
    public void logout() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        SharedPreferences sharedPreferences2 = mCtx.getSharedPreferences(SHARED_PREF_NAME1, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor2 = sharedPreferences2.edit();
        editor2.clear();
        editor2.apply();
    }


}


