package com.shazly.task.api;


import com.shazly.task.ui.login.LoginModel;
import com.shazly.task.ui.main.fragment.home.ProductsModel;
import com.shazly.task.ui.main.fragment.home.SoldModel;
import com.shazly.task.ui.main.fragment.profile.UserModel;


import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetroService {


    @FormUrlEncoded
    @POST("auth")
    Single<LoginModel> login(@Field("username") String username,
                             @Field("password") String password);


    @GET("sold-items")
    Single<SoldModel> getSoldCount();

    @GET("products")
    Single<ProductsModel> getProductsCount();

    @GET("profile-show")
    Single<UserModel> getprofile();

    @GET("employee-statistics")
    Single<UserModel> getEmployee();


}
