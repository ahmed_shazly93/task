package com.shazly.task.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ObjectBaseReasponse<T> {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    private T response;
    @SerializedName("notifications_count")
    @Expose
    private int notifications_count;


    public ObjectBaseReasponse() {
    }

    public ObjectBaseReasponse(Boolean status, String message, T response, int notifications_count) {
        this.status = status;
        this.message = message;
        this.response = response;
        this.notifications_count = notifications_count;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }

    public int getNotifications_count() {
        return notifications_count;
    }

    public void setNotifications_count(int notifications_count) {
        this.notifications_count = notifications_count;
    }
}
