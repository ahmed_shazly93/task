package com.shazly.task.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ArrayBaseResponse<T>{

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    private ArrayList<T> response;

    public ArrayBaseResponse() {
    }

    public ArrayBaseResponse(Boolean status, String message, ArrayList<T> response) {
        this.status = status;
        this.message = message;
        this.response = response;
    }

    public ArrayList<T> getResponse() {
        return response ;
    }

    public void setResponse(ArrayList<T> response) {
        this.response = response;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
